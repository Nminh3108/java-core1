package assignment1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Program {
    public static void main(String[] args) {
        System.out.println("Khoi tao gia tri cho 1 doi tuong Account");
        //-------------- Khoi tao gia tri cho doi tuong DEPARTMENT ------------------
        Department department1 = new Department();
        department1.departmentId = 1;
        department1.departmentName = "Phong Ban 1 ";

        Department department2 = new Department();
        department2.departmentId = 2;
        department2.departmentName = "Phong Ban 2 ";

        Department department3 = new Department();
        department3.departmentId = 3;
        department3.departmentName = "Phong Ban 3 ";
//-------------- Khoi tao gia tri xcho doi tuong POSITION ------------------
        Position position1 = new Position();
        position1.positionId = 1;
        position1.positionName = PositionName.DEV;

        Position position2 = new Position();
        position2.positionId = 2;
        position2.positionName = PositionName.DEV;

        Position position3 = new Position();
        position3.positionId = 3;
        position3.positionName = PositionName.DEV;
        //-------------- Khoi tao gia tri cho doi tuong ACCOUNT ------------------
        Group group1 = new Group();

        Group group2 = new Group();

        Group group3 = new Group();

        Group[] arrGr1 = {group1, group2, group3};
//-------------- Khoi tao gia tri cho doi tuong ACCOUNT ------------------

        Account account1 = new Account();
        account1.accountId = 1;
        account1.setDepartmentId(1);
        account1.email = "abc@gmail.com";
        account1.username = "Username 1 ";
        account1.fullName = "Nguyen Van A ";
        account1.positionId = 1;
        account1.createDate = new Date();

        Account account2 = new Account();
        account2.accountId = 2;
        account2.setDepartmentId(2);
        account2.email = "abc2@gmail.com";
        account2.username = "Username 2 ";
        account2.fullName = "Nguyen Van B ";
        account2.positionId = 2;
        account2.createDate = new Date();

        Account account3 = new Account();
        account3.accountId = 3;
        account3.setDepartmentId(3);
        account3.email = "abc3@gmail.com";
        account3.username = "Username 3 ";
        account3.fullName = "Nguyen Van C ";
        account3.positionId = 3;
        account3.createDate = new Date();

        System.out.println(account1.email);
        System.out.println(account1.username);

        //-------------- Khoi tao gia tri cho danh sach account ------------------
        Account[] arrAccount1 = {account1, account2, account3};

        group1.accounts = arrAccount1;

        //-------------- Khoi tao gia tri cho Exam ------------------
        Exam exam = new Exam();
        Exam exam1 = null;
        exam1.createDate = new Date();
















        // ------------------------ assignment2 ------------------------
        System.out.println("-------- Question 1 ;---------");
        if (account2.department == null) {
            System.out.println("Nhan vien nay chua co phong ban");
        } else {
            System.out.println("Phòng ban của nv này là: " + account2.department.departmentName);


            System.out.println("-------- Question 2 ;---------");
            if (account2.group.length < 3) {
                System.out.println("Group cua nhan vien nay la Java Fresher, C# Fresher");
            } else if (account2.group.length == 3) {
                System.out.println("Nhan vien nay la nguoi quan trong nhat ");
            } else if (account2.group.length == 3) {
                System.out.println("Nhan vien nay la nguoi hong chuyen ");
            } else {
                System.out.println(" Cac truong hop con lai ");
            }


        System.out.println("-------- Question 10 ;---------");
        Account[] arrAccount2 = {account1, account3};

        // Cach 1 ; Dung for each
        int stt = 1;
        for (Account account : arrAccount2) {
            System.out.println("------------");
            System.out.println("Thong tin cua account thu" + account.accountId + " la: ");
            System.out.println("Email: " + account.email);
            System.out.println("FUll name: " + account.fullName);
            System.out.println("Phong ban: " + account.position.positionName);
            stt++;
        }
        Account account = arrAccount2[2];
        // Cach 2; dung for i
        for (int i = 0; i < arrAccount2.length; i++) {
            System.out.println("------------");
            System.out.println("Thong tin cua account thu" + (i + 1) + " la: ");
            System.out.println("Email: " + account.email);
            System.out.println("FUll name: " + account.fullName);
            System.out.println("Phong ban: " + account.position.positionName);

        }
        System.out.println("-------- Question 16 ;---------");

        int number = 0;
        while (number < arrAccount2.length) {
            System.out.println("------------");
            System.out.println("Thong tin cua account thu" + (number + 1) + " la: ");
            System.out.println("Email: " + arrAccount2[number].email);
            System.out.println("FUll name: " + arrAccount2[number].fullName);
            System.out.println("Phong ban: " + arrAccount2[number].position.positionName);
            number++;
        }

        // cach 2 ; dung do while
        int number2 = 0;
        do {
            System.out.println("------------");
            System.out.println("Thong tin cua account thu" + (number2 + 1) + " la: ");
            System.out.println("Email: " + arrAccount2[number2].email);
            System.out.println("FUll name: " + arrAccount2[number2].fullName);
            System.out.println("Phong ban: " + arrAccount2[number2].position.positionName);
            number2++;
        } while (number2 < (arrAccount2.length - 1));

        System.out.println("-------- Question 3 ;---------");

        System.out.println(account2.department == null ? "Nhan vien nay chua co phong ban." : "Phong cua nhan vien nay la: " + account2.department.departmentName);


        System.out.println("---------Question 4;--------- ");

        System.out.println(account1.position.positionName.toString() == "Dev" ? "Day la Developer" : "Nguoi này ko phai la Developer");

        System.out.println("-------- Question 5 ;---------");
        int accountLength = group1.accounts.length;
        System.out.println(accountLength);
        switch (accountLength) {
            case 1:
                System.out.println("Nhom co 1 thanh vien");
                break;
            case 2:
                System.out.println("Nhom co 2 thanh vien");
                break;
            case 3:
                System.out.println("Nhom co 3 thanh vien");
                break;
            default:
                System.out.println("Nhom co nhieu thanh vien ");
                break;
        }
    }


        System.out.println(" ---------Question 6; ---------");

        if (account2.group == null) {
            System.out.println("Nhan vien nay chua ca group");
        } else {
            switch (account2.group.length) {
                case 1:
                    System.out.println("Group cua nhan vien nay la Java Fresher, C# Fresher");
                    break;
                case 2:
                    System.out.println("Nhan vien nay la nguoi hong chuyen");
                    break;
                case 3:
                    System.out.println("Nhan vien nay la nguoi quan trong, tham gia nhieu group");
                    break;
            }
        }


        System.out.println("---------Question 7;---------");

                String positionName = account1.position.positionName.toString();
        switch (positionName) {
            case "Dev":
                System.out.println("Day la Developer");
                break;
            default:
                System.out.println("Nguoi nay khong phai la Developer");
                break;}



        System.out.println("---------FOREACH Question 8;---------");
        Account[] accArray = { account1, account2, account3 };
        for (Account account : accArray) {
            System.out.println("AccountID: " + account.accountId + " Email: " + account.email + " Name: " + account.fullName);

        }



        System.out.println("---------Question 9; ---------");
        Department[] depArray = { department1, department2, department3 };
        for (Department department : depArray) {
            System.out.println("DepID: " + department.departmentId + " Name: " + department.departmentName);
        }



        System.out.println("----------Question 13; ------------");
        Account[] accArray2 = { account1, account2, account3 };
        for (int g = 0; g < accArray2.length; g++) {
                System.out.println("Thong tin account thu " + (g + 1) + " là:");
                System.out.println("Email: " + accArray2[g].email);
                System.out.println("Full name: " + accArray2[g].fullName);
                System.out.println("Phong ban: " + accArray2[g].department.departmentName);

        }


        System.out.println("---------- Question 14;-----------");

                Account[] accArray3 = { account1, account2, account3 };
        for (int k = 0; k < accArray3.length; k++) {
            if (accArray3[k].accountId < 4) {
                System.out.println("Thong tin account thu " + (k + 1) + " là:");
                System.out.println("Email: " + accArray3[k].email);
                System.out.println("Full name: " + accArray3[k].fullName);
                System.out.println("Phong ban: " + accArray3[k].department.departmentName);

            }
        }



        // Exercise 3 : Date Format
        System.out.println("---------- Question 14;-----------");

        Locale locale = new Locale( "vi" , "Vn" );
        DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String createDate= dateFormat1.format(exam1.createDate);
        System.out.println(createDate);


        System.out.println("---------- Question 2;-----------");
        DateFormat dateFormat = new SimpleDateFormat(  "yyyy-MM-dd-HH-mm-ss" ) ;
        String createDate2 = dateFormat.format(exam1.createDate);
        System.out.println(createDate2);


    }
}