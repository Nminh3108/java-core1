package assignment1;

import java.io.OptionalDataException;
import java.util.Date;

public class Account {
    public int positionId;
    public OptionalDataException group;
    private int departmentId;
    int accountId ;
    String email ;
    String username ;
    String fullName ;
    Department department ;
    Position position;
    Date createDate ;

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }
}
