package bai2;

public class DempFor {
    public static void main(String[] args0) {
        // For 1
        for (int i = 0 ; i <100 ; i++) {
             System.out.println(i);
        }


        // for each ( chi su dung voi 1 danh sach)
        int[] arri = {1,2,3,4,5};
        // for ( kieu du lieu cua tung phan tu trong ds, dat ten cho moi phan tu ,khai bao danh sach du lieu ma minh muon su dung )
        for (int item : arri ){
            // xu ly theo tung phan tu ma no duoc goi toi
            System.out.println(item);
        }
    }
}
