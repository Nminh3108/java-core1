package bai2;

public class DemoWhile {
    public static void main(String[] args0) {
        // Kiem tra dieu kien truoc -> hanh dong
        System.out.println("Demo While");
        int i=1 ;

        while (i<10){
            System.out.println(i);
            i++ ;
        }
        // do-while : thuc hien hanh dong trong do truoc -> kiem tra dieu kien
        System.out.println("Demo do while");
        int abc = 0;
        do {
            abc++ ;
            System.out.println(abc );
        } while (abc <5) ;

    }
}
