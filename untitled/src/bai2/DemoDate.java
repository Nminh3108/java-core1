package bai2;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DemoDate {
    public static void main(String[] args0) throws ParseException {
        // dinh dang theo pattern
        Date date = new Date();
        System.out.println(date);
        // tao dinh dang ngay thang nam in ra man hinh
        DateFormat dateFormat = new SimpleDateFormat(  "yyyy-MM-dd HH:mm:ss" ) ;
        String StringDate = dateFormat.format(date);
        System.out.println(StringDate);


// dinh dnag ngay thang nam theo khu vuc
        Locale locale = new Locale( "vi" , "Vn" );
        DateFormat dateFormat1 = DateFormat.getDateInstance(DateFormat.DEFAULT, locale);
        String stringDate2 = dateFormat1.format(date);
        System.out.println(stringDate2);

        // chuyen doi tu chuoi sang kieu du lieu Date
        String dateString  = "24-02-2023 20:15:30" ;
        DateFormat dateFormat2 = new SimpleDateFormat(  "yyyy-MM-dd HH:mm:ss" ) ;
        Date date2 = dateFormat2.parse(dateString);
        System.out.println(date2);
    }
}
