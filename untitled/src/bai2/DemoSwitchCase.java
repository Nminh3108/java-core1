package bai2;
public class DemoSwitchCase {
    public static void main(String[] args0) {
        int number = 2;

        switch (number) {
            case 1: // cac gia tri de so sanh voi bien truyen vao la number
                System.out.println("Truong hop 1");
                break;
            case 2:
                System.out.println("Truong hop 2");
                break;
            case 3:
                System.out.println("Truong hop 3");
                break;
            default:
                System.out.println("Cac truong hop con lai ");
                break;
        }

    }
}