package bai2;

public class DemoBreak {
    public static void main(String[] args0) {

        for (int i = 0; i < 10; i++) {
            System.out.println("Gia tri cua i la : " + i);

            for (int j = 0; j < 5; j++) {
                System.out.println("Gia tri cua j la : " + i);
                if (j == 2) {
                    System.out.println("i=2, check dieu kien tiep theo, ko xuong duoi nua");
                    continue;
                }

                System.out.println("Chua gap continue");

                if (j == 3) {
                    System.out.println("j=3, ko thuc hien vong lap voi j nua");
                    break;
                }
                if(j==4){
                    System.out.println("j=4, dung chuong trinh");
                    return;
                }
                System.out.println("Gia tri cua j la: " + j);
            }
        }
    }
    }

