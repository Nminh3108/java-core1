package bai2;

import java.time.LocalDate;
import java.util.Random;

public class RandomDate {
    public static void main(String[] args0) {
        Random random = new Random();
        int minDate = (int) LocalDate.of(1990,1,1).toEpochDay();
        int maxDate = (int) LocalDate.now().toEpochDay();
        long randomLong = minDate+ random.nextInt((int) (maxDate - minDate));



        LocalDate randomDate = LocalDate.ofEpochDay(randomLong);
        System.out.println(randomDate);

    }
}

