package Assignment2;

import java.time.LocalDate;
import java.util.Random;

public class ex4 {
    public static void main(String[] args) {
        //----------------- ex4--------------
        System.out.println("Ex4: random number");
        System.out.println("----------ques1--------");
        Random random = new Random();
        int number = random.nextInt();
        System.out.println("so nguyen bat ky la " + number );

        System.out.println("------ques2----");
        float number2 = random.nextFloat();
        System.out.println("so thuc bat ky la :" + number2 );

        System.out.println("------ques3----");
        String[] arrName = {"Nguyen Van A", "Nguyen Van B" , "Nguyen Van C"} ;
        int numberRandom3 = random.nextInt(2);
        String name = arrName[0];
        String name2 = arrName[1];
        String name3 = arrName[2];
        System.out.println(name);

        System.out.println("----ques4-----");
        int minDate = (int) LocalDate.of(1995,7,24).toEpochDay();
        int maxDate = (int) LocalDate.of(1995,12,20).toEpochDay();
        long randomLong = minDate+ random.nextInt((int) (maxDate - minDate));
        LocalDate localDate = LocalDate.ofEpochDay(randomLong);
        System.out.println(localDate);

        System.out.println("-------ques5-----");
    }
}
